###############################################################################
#
# tcsh
#
###############################################################################

TCSH_VERSION = 6.24.00
TCSH_SOURCE = tcsh-$(TCSH_VERSION).tar.gz
TCSH_SITE = https://www.emulab.net/downloads/linux-mfs-sources
TCSH_LICENSE = BSD
TCSH_LICENSE_FILES = Copyright
TCSH_INSTALL_STAGING = NO
TCSH_INSTALL_TARGET = YES
TCSH_CONF_OPTS =

# Instead of patching the Makefile to avoid this egregious behavior
# (attempting to save off the currently-installed tcsh binary), just
# work around it via buildroot's hook mechanism.
define TCSH_POST_BUILD_HACK
	touch $(TARGET_DIR)/usr/bin/tcsh
endef
TCSH_POST_BUILD_HOOKS += TCSH_POST_BUILD_HACK
define TCSH_POST_INSTALL_HACK
	rm -f $(TARGET_DIR)/usr/bin/tcsh.old
endef
TCSH_POST_INSTALL_TARGET_HOOKS += TCSH_POST_INSTALL_HACK

# Make various users/things happy by providing a csh symlink.
define TCSH_POST_INSTALL_SYMLINK_CSH
	if [ ! -e $(TARGET_DIR)/bin/tcsh ]; then cd $(TARGET_DIR)/bin && ln -sf ../usr/bin/tcsh tcsh ; fi
	if [ ! -e $(TARGET_DIR)/bin/csh ]; then cd $(TARGET_DIR)/bin && ln -sf ../usr/bin/tcsh csh ; fi
endef
TCSH_POST_INSTALL_TARGET_HOOKS += TCSH_POST_INSTALL_SYMLINK_CSH

$(eval $(autotools-package))
