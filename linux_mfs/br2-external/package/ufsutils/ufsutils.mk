################################################################################
#
# ufsutils
#
################################################################################

UFSUTILS_VERSION = 8.2
UFSUTILS_SOURCE = ufsutils-$(UFSUTILS_VERSION).tar.gz
UFSUTILS_SITE = https://www.emulab.net/downloads/linux-mfs-sources
UFSUTILS_LICENSE = BSD-2c
UFSUTILS_LICENSE_FILES = COPYING
UFSUTILS_DEPENDENCIES = libbsd libedit
UFSUTILS_INSTALL_STAGING = NO
UFSUTILS_MAKE_OPTS = LIB_type=static
UFSUTILS_MAKE = $(MAKE1)
TARGET_CONFIGURE_OPTS += CFLAGS="$(TARGET_CFLAGS) -fcommon"

define UFSUTILS_BUILD_CMDS
    $(MAKE1) WITH_INTERNAL_FSTAB=1 $(UFSUTILS_MAKE_OPTS) $(TARGET_CONFIGURE_OPTS) -C $(@D) all
endef

define UFSUTILS_INSTALL_TARGET_CMDS
    $(MAKE1) DESTDIR=$(TARGET_DIR) -C $(@D)/sbin/badsect install
    $(MAKE1) DESTDIR=$(TARGET_DIR) -C $(@D)/sbin/dumpfs install
    $(MAKE1) DESTDIR=$(TARGET_DIR) -C $(@D)/sbin/ffsinfo install
    $(MAKE1) DESTDIR=$(TARGET_DIR) -C $(@D)/sbin/fsck_ffs install
    $(MAKE1) DESTDIR=$(TARGET_DIR) -C $(@D)/sbin/fsdb install
    $(MAKE1) DESTDIR=$(TARGET_DIR) -C $(@D)/sbin/growfs install
    $(MAKE1) DESTDIR=$(TARGET_DIR) -C $(@D)/sbin/newfs install
    $(MAKE1) DESTDIR=$(TARGET_DIR) -C $(@D)/sbin/tunefs install
endef

$(eval $(generic-package))
