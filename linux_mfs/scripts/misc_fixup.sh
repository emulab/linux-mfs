#! /bin/sh

target_dir="$1"

if [ -z "$target_dir" ] || ! [ -d "$target_dir" ]; then
	echo "${0##*/}: invalid target directory \"$target_dir\""
	exit 1
fi

if [ ! -f "$target_dir/bin/tcsh" ] && [ ! -L "$target_dir/bin/tcsh" ]; then
	ln -sf /bin/tcsh.fake "$target_dir/bin/tcsh"
	ln -sf /bin/tcsh.fake "$target_dir/bin/csh"
fi

if [ ! -f "$target_dir/bin/bash" ] && [ ! -L "$target_dir/bin/bash" ]; then
	ln -sf /bin/bash.fake "$target_dir/bin/bash"
fi

if [ ! -L "$target_dir/etc/mtab" ]; then
	touch "$target_dir/etc/mtab"
fi

mkdir -p $target_dir/users $target_dir/groups $target_dir/proj

rm -rf $target_dir/usr/share/man
