#! /bin/sh

target_dir="$1"

if [ -z "$target_dir" ] || ! [ -d "$target_dir" ]; then
	echo "${0##*/}: invalid target directory \"$target_dir\""
	exit 1
fi

chmod 1777 "$target_dir/tmp"
chmod 1777 "$target_dir/var/tmp"
chmod 1777 "$target_dir/var/lock"
chmod 0700 "$target_dir/root/.ssh"
chmod 0600 "$target_dir/root/.ssh/authorized_keys"
chmod 0600 "$target_dir/etc/shadow"
chmod u+s "$target_dir/bin/busybox"
if [ -e "$target_dir/usr/bin/sudo" ]; then
    chmod u+s "$target_dir/usr/bin/sudo"
fi
if [ -e "$target_dir/etc/sudoers" ]; then
    chmod 0440 "$target_dir/etc/sudoers"
fi
if [ -e "$target_dir/etc/sudoers.d" ]; then
    chmod 0770 "$target_dir/etc/sudoers.d"
    if [ -e "$target_dir/etc/sudoers.d/99-emulab" ]; then
	chmod 0750 "$target_dir/etc/sudoers.d/99-emulab"
    fi
fi
chmod 755 "$target_dir/bin"
chmod 755 "$target_dir/etc"
chmod 755 "$target_dir/etc/default"
chmod 755 "$target_dir/etc/emulab"
chmod 755 "$target_dir/etc/init.d"
chmod 755 "$target_dir/etc/network"
chmod 755 "$target_dir/sbin"
chmod 755 "$target_dir/usr"
chmod 755 "$target_dir/usr/share"
chmod 755 "$target_dir/usr/share/udhcpc"
