#
# Since buildroot doesn't expose `make oldconfig`, we cannot start from a
# full kernel config generated as part of a previous build, and use that to
# update to a newer kernel version while leveraging the normal kernel build
# `oldconfig` target to prompt us for new or modified option values.  Doing
# so is crucial to our ability to keep the build small by keeping kernel
# configuration limited to the options we require.
#
LINUX_KCONFIG_EDITORS += oldconfig
