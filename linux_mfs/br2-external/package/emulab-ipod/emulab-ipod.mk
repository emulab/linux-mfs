################################################################################
#
# emulab-ipod
#
################################################################################

EMULAB_IPOD_VERSION = 3.4.0
EMULAB_IPOD_SOURCE = emulab-ipod-$(EMULAB_IPOD_VERSION).tar.gz
EMULAB_IPOD_SITE = https://www.emulab.net/downloads/linux-mfs-sources
EMULAB_IPOD_LICENSE = AGPL

EMULAB_IPOD_MAKE_OPTS = V=1 KVERSION=$(LINUX_VERSION_PROBED)

$(eval $(kernel-module))
$(eval $(generic-package))
