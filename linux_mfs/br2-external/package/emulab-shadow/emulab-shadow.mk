################################################################################
#
# shadow
#
################################################################################

EMULAB_SHADOW_VERSION = 4.6
EMULAB_SHADOW_SOURCE = shadow-$(EMULAB_SHADOW_VERSION).tar.gz
#EMULAB_SHADOW_SITE = https://github.com/shadow-maint/shadow/releases/download/$(EMULAB_SHADOW_VERSION)
EMULAB_SHADOW_SITE = https://www.emulab.net/downloads/linux-mfs-sources
EMULAB_SHADOW_LICENSE = BSD-3c
EMULAB_SHADOW_LICENSE_FILES = COPYING
EMULAB_SHADOW_DEPENDENCIES = acl attr busybox

EMULAB_SHADOW_INSTALL_STAGING = NO

# --enable-subordinate-ids=no is to disable a test program build
# The setting of ENABLE_SUBIDS in config.h re-enables this option.
EMULAB_SHADOW_CONF_OPTS = --disable-nls --enable-subordinate-ids=no

#define EMULAB_SHADOW_POST_PATCH_DISABLE_SUID
#	# Disable setting suid bit when installing
#	$(SED) 's/\(^suidu*bins = \).*/\1/' $(@D)/src/Makefile.in
#endef
#EMULAB_SHADOW_POST_PATCH_HOOKS += EMULAB_SHADOW_POST_PATCH_DISABLE_SUID

define EMULAB_SHADOW_POST_CONFIGURE_ENABLE_SUBIDS
	echo "#define ENABLE_SUBIDS 1" >> $(@D)/config.h
endef
EMULAB_SHADOW_POST_CONFIGURE_HOOKS += EMULAB_SHADOW_POST_CONFIGURE_ENABLE_SUBIDS

# Shadow configuration to support audit
ifeq ($(BR2_PACKAGE_AUDIT),y)
EMULAB_SHADOW_DEPENDENCIES += audit
EMULAB_SHADOW_CONF_OPTS += --with-audit=yes
endif

# Shadow with linux-pam support
ifeq ($(BR2_PACKAGE_LINUX_PAM),y)
EMULAB_SHADOW_DEPENDENCIES += linux-pam
EMULAB_SHADOW_CONF_OPTS += --with-libpam=yes

# Comment out all config entries that conflict with using PAM
define EMULAB_SHADOW_LOGIN_CONFIGURATION
	for FUNCTION in FAIL_DELAY FAILLOG_ENAB LASTLOG_ENAB MAIL_CHECK_ENAB \
		OBSCURE_CHECKS_ENAB PORTTIME_CHECKS_ENAB QUOTAS_ENAB CONSOLE MOTD_FILE \
		FTMP_FILE NOLOGINS_FILE ENV_HZ PASS_MIN_LEN SU_WHEEL_ONLY CRACKLIB_DICTPATH \
		PASS_CHANGE_TRIES PASS_ALWAYS_WARN CHFN_AUTH ENCRYPT_METHOD ENVIRON_FILE ; \
	do \
		sed -i "s/^$${FUNCTION}/# &/" $(TARGET_DIR)/etc/login.defs ; \
	done
endef
EMULAB_SHADOW_POST_INSTALL_TARGET_HOOKS += EMULAB_SHADOW_LOGIN_CONFIGURATION
endif

# Shadow with selinux support
ifeq ($(BR2_PACKAGE_LIBSELINUX),y)
EMULAB_SHADOW_DEPENDENCIES += libselinux libsemanage
EMULAB_SHADOW_CONF_OPTS += --with-selinux=yes
endif

$(eval $(autotools-package))
