###############################################################################
#
# jove
#
###############################################################################

JOVE_VERSION = 4.16.0.72
JOVE_SOURCE = jove-$(JOVE_VERSION).tar.gz
JOVE_SITE = https://www.emulab.net/downloads/linux-mfs-sources
JOVE_LICENSE = BSD
JOVE_LICENSE_FILES = Copyright
JOVE_INSTALL_STAGING = NO
JOVE_INSTALL_TARGET = YES
JOVE_CONF_OPTS =

JOVE_LIB_DIR = $(TARGET_DIR)/usr/lib/jove

define JOVE_BUILD_CMDS
	$(MAKE) $(TARGET_CONFIGURE_OPTS) -C $(@D)
endef

define JOVE_INSTALL_TARGET_CMDS
	$(INSTALL) -D -m 0755 $(@D)/jjove $(TARGET_DIR)/usr/bin/jove
	$(INSTALL) -D -d -m 0755 $(JOVE_LIB_DIR)
	$(INSTALL) -D -m 0644 $(@D)/doc/jove.rc.vt100 $(JOVE_LIB_DIR)/
	$(INSTALL) -D -m 0644 $(@D)/doc/jove.rc.xterm $(JOVE_LIB_DIR)/
	ln -srf $(JOVE_LIB_DIR)/jove.rc.xterm \
		$(JOVE_LIB_DIR)/jove.rc.xterm-color
endef

$(eval $(generic-package))
