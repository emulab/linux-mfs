################################################################################
#
# rng-tools-modern
#
################################################################################

RNG_TOOLS_MODERN_VERSION = 6.9
RNG_TOOLS_MODERN_SITE = http://www.emulab.net/downloads/linux-mfs-sources
RNG_TOOLS_MODERN_SOURCE = rng-tools-$(RNG_TOOLS_MODERN_VERSION).tar.gz
RNG_TOOLS_MODERN_LICENSE = GPL-2.0
RNG_TOOLS_MODERN_LICENSE_FILES = COPYING

RNG_TOOLS_MODERN_AUTORECONF = YES

# Work around for uClibc or musl toolchains which lack argp_*()
# functions.
ifeq ($(BR2_PACKAGE_ARGP_STANDALONE),y)
RNG_TOOLS_MODERN_CONF_ENV += LIBS="-largp" CFLAGS="-I$(TARGET_DIR)/usr/include"
RNG_TOOLS_MODERN_DEPENDENCIES += argp-standalone
endif

ifeq ($(BR2_PACKAGE_LIBGCRYPT),y)
RNG_TOOLS_MODERN_DEPENDENCIES += libgcrypt
else
RNG_TOOLS_MODERN_CONF_OPTS += --without-libgcrypt
endif

RNG_TOOLS_MODERN_CONF_OPTS += --without-nistbeacon --without-pkcs11 --without-libsysfs

define RNG_TOOLS_MODERN_INSTALL_INIT_SYSV
	$(INSTALL) -D -m 755 package/rng-tools/S21rngd \
		$(TARGET_DIR)/etc/init.d/S21rngd
endef

define RNG_TOOLS_MODERN_INSTALL_INIT_SYSTEMD
	$(INSTALL) -D -m 644 package/rng-tools/rngd.service \
		$(TARGET_DIR)/usr/lib/systemd/system/rngd.service
	mkdir -p $(TARGET_DIR)/etc/systemd/system/multi-user.target.wants
	ln -fs ../../../../usr/lib/systemd/system/rngd.service \
		$(TARGET_DIR)/etc/systemd/system/multi-user.target.wants/rngd.service
endef

define RNG_TOOLS_MODERN_AUTOGEN_HOOK
#	cd $(@D) && ./autogen.sh
	cd $(@D) && cp -p README.md README
endef
RNG_TOOLS_MODERN_PRE_CONFIGURE_HOOKS += RNG_TOOLS_MODERN_AUTOGEN_HOOK

$(eval $(autotools-package))

#define RNG_TOOLS_MODERN_INSTALL_TARGET_CMDS
#	$(INSTALL) -D -m 755 $(@D)/rngd \
#		$(TARGET_DIR)/usr/sbin/rngd
#endef
