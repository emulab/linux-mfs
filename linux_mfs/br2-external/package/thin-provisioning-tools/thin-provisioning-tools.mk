################################################################################
#
# thin-provisioning-tools
#
################################################################################

THIN_PROVISIONING_TOOLS_VERSION = 0.9.0
THIN_PROVISIONING_TOOLS_SOURCE = thin-provisioning-tools-$(THIN_PROVISIONING_TOOLS_VERSION).tar.gz
THIN_PROVISIONING_TOOLS_SITE = https://www.emulab.net/downloads/linux-mfs-sources
THIN_PROVISIONING_TOOLS_LICENSE = GPLv3
THIN_PROVISIONING_TOOLS_LICENSE_FILES = COPYING
THIN_PROVISIONING_TOOLS_DEPENDENCIES = libaio expat boost
THIN_PROVISIONING_TOOLS_INSTALL_STAGING = NO
THIN_PROVISIONING_TOOLS_AUTORECONF = YES
#THIN_PROVISIONING_TOOLS_MAKE_OPTS = LIB_type=static
#THIN_PROVISIONING_TOOLS_MAKE = $(MAKE1)

#define THIN_PROVISIONING_TOOLS_BUILD_CMDS
#    $(MAKE1) WITH_INTERNAL_FSTAB=1 $(THIN_PROVISIONING_TOOLS_MAKE_OPTS) $(TARGET_CONFIGURE_OPTS) -C $(@D) all
#endef

#define THIN_PROVISIONING_TOOLS_INSTALL_TARGET_CMDS
#    $(MAKE1) DESTDIR=$(TARGET_DIR) -C $(@D)/sbin/badsect install
#    $(MAKE1) DESTDIR=$(TARGET_DIR) -C $(@D)/sbin/dumpfs install
#    $(MAKE1) DESTDIR=$(TARGET_DIR) -C $(@D)/sbin/ffsinfo install
#    $(MAKE1) DESTDIR=$(TARGET_DIR) -C $(@D)/sbin/fsck_ffs install
#    $(MAKE1) DESTDIR=$(TARGET_DIR) -C $(@D)/sbin/fsdb install
#    $(MAKE1) DESTDIR=$(TARGET_DIR) -C $(@D)/sbin/growfs install
#    $(MAKE1) DESTDIR=$(TARGET_DIR) -C $(@D)/sbin/newfs install
#    $(MAKE1) DESTDIR=$(TARGET_DIR) -C $(@D)/sbin/tunefs install
#endef

$(eval $(autotools-package))
