################################################################################
#
# kexec
#
################################################################################

KEXEC_PPC64_VERSION = 2.0.17
KEXEC_PPC64_SOURCE = kexec-tools-$(KEXEC_PPC64_VERSION).tar.xz
KEXEC_PPC64_SITE = $(BR2_KERNEL_MIRROR)/linux/utils/kernel/kexec
KEXEC_PPC64_LICENSE = GPL-2.0
KEXEC_PPC64_LICENSE_FILES = COPYING

# Makefile expects $STRIP -o to work, so needed for !BR2_STRIP_strip
KEXEC_PPC64_MAKE_OPTS = STRIP="$(TARGET_CROSS)strip"

ifeq ($(BR2_PACKAGE_KEXEC_PPC64_ZLIB),y)
KEXEC_PPC64_CONF_OPTS += --with-zlib
KEXEC_PPC64_DEPENDENCIES = zlib
else
KEXEC_PPC64_CONF_OPTS += --without-zlib
endif

ifeq ($(BR2_PACKAGE_XZ),y)
KEXEC_PPC64_CONF_OPTS += --with-lzma
KEXEC_PPC64_DEPENDENCIES += xz
else
KEXEC_PPC64_CONF_OPTS += --without-lzma
endif

define KEXEC_PPC64_REMOVE_LIB_TOOLS
	rm -rf $(TARGET_DIR)/usr/lib/kexec-tools
endef

KEXEC_PPC64_POST_INSTALL_TARGET_HOOKS += KEXEC_PPC64_REMOVE_LIB_TOOLS

$(eval $(autotools-package))
