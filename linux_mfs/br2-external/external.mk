include $(BR2_EXTERNAL_EMULAB_LINUX_MFS_PATH)/package/emulab-shadow/*.mk
include $(BR2_EXTERNAL_EMULAB_LINUX_MFS_PATH)/package/jove/*.mk
include $(BR2_EXTERNAL_EMULAB_LINUX_MFS_PATH)/package/tcsh/*.mk
include $(BR2_EXTERNAL_EMULAB_LINUX_MFS_PATH)/package/emulab-ipod/*.mk
include $(BR2_EXTERNAL_EMULAB_LINUX_MFS_PATH)/package/kexec-ppc64/*.mk
include $(BR2_EXTERNAL_EMULAB_LINUX_MFS_PATH)/package/emulab-linux-firmware/*.mk
include $(BR2_EXTERNAL_EMULAB_LINUX_MFS_PATH)/package/rng-tools-modern/*.mk
include $(BR2_EXTERNAL_EMULAB_LINUX_MFS_PATH)/package/ufsutils/*.mk
include $(BR2_EXTERNAL_EMULAB_LINUX_MFS_PATH)/package/thin-provisioning-tools/*.mk

#
# This basically works because the buildroot-<version>/linux/linux.mk
# Makefile is kind to us.  (Buildroot doesn't define any generic,
# guaranteed-to-work per-package overrides (other than the specific
# ability to control where the package source code comes from).  To
# override an existing package, you must change the name and add it in
# this external buildroot tree.)
#
# NM: this is commented out in favor of setting BR2_JLEVEL=0 in the
# various defconfigs; we want parallel builds (and thus don't care about
# reproducible builds) for now.  BR2_JLEVEL autodetects n cpus and runs
# `make -jn` as appropriate.
#
#LINUX_MAKE_FLAGS += -j$(shell grep processor /proc/cpuinfo | wc -l)

#
# Add a custom kernel hook to generate a uImage from the kernel image if
# aarch64.
#
define LINUX_AARCH64_BUILD_UIMAGE
	[ $(BR2_ARCH) = "aarch64" ] && (gzip -c $(BINARIES_DIR)/Image > $(BINARIES_DIR)/Image.gz && mkimage -A arm -O linux -T kernel -n "Emulab ${BUILDROOT_VERSION} MFS kernel" -d $(BINARIES_DIR)/Image.gz $(BINARIES_DIR)/uImage) || true
endef
LINUX_POST_INSTALL_IMAGES_HOOKS += LINUX_AARCH64_BUILD_UIMAGE
