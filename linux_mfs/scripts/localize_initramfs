#!/bin/sh

if [ ! `id -u` -eq 0 ]; then
    echo "ERROR: must run as real root!"
    exit 1
fi

PUBKEY=`echo "select value from sitevariables where name='node/ssh_pubkey'" | mysql -N tbdb`

./extract_initramfs

mkdir -p extracted_initramfs/root/.ssh
chmod 700 extracted_initramfs/root/.ssh
touch extracted_initramfs/root/.ssh/authorized_keys
chmod 600 extracted_initramfs/root/.ssh/authorized_keys
if [ -n "$PUBKEY" ]; then
    echo "$PUBKEY" >> extracted_initramfs/root/.ssh/authorized_keys
else
    cat ~/.ssh/id_rsa.pub >> extracted_initramfs/root/.ssh/authorized_keys
fi

# Signal to the scripts in the MFS that our root dir is localized, so that
# the MFS can localize images it deploys.
touch extracted_initramfs/root/.localized

mkdir -p extracted_initramfs/etc/emulab
cp -p /usr/testbed/etc/emulab.pem extracted_initramfs/etc/emulab/
cp -p /usr/testbed/etc/client.pem extracted_initramfs/etc/emulab/
chown 0:0 extracted_initramfs/etc/emulab/emulab.pem
chown 0:0 extracted_initramfs/etc/emulab/client.pem

mkdir -p extracted_initramfs/etc
cp -p /etc/localtime extracted_initramfs/etc/
chown 0:0 extracted_initramfs/etc/localtime

mkdir -p extracted_initramfs/etc/ssh
cp -p /usr/testbed/etc/image_hostkeys/ssh_host_ecdsa_key* extracted_initramfs/etc/ssh/
cp -p /usr/testbed/etc/image_hostkeys/ssh_host_ed25519_key* extracted_initramfs/etc/ssh/
cp -p /usr/testbed/etc/image_hostkeys/ssh_host_rsa_key* extracted_initramfs/etc/ssh/
cp -p /usr/testbed/etc/image_hostkeys/ssh_host_dsa_key* extracted_initramfs/etc/ssh/
chown 0:0 extracted_initramfs/etc/ssh/ssh_host_*key*
if [ -e emulab-bossnode ]; then
    mkdir -p extracted_initramfs/etc/emulab
    cat emulab-bossnode > extracted_initramfs/etc/emulab/bossnode
fi

# Signal to the scripts in the MFS that we are localized, so that
# the MFS can localize images it deploys.
touch extracted_initramfs/.localized

./compress_initramfs
