MFS_ARCH		= x86_64
DEFCONFIG_POSTFIX	=
BUILDROOT_MAJOR_VERSION	= 2024.02
BUILDROOT_VERSION	= $(BUILDROOT_MAJOR_VERSION).10

SOURCE_PATH		= $(TOPDIR)/source
SCRIPTS_PATH		= $(TOPDIR)/scripts
BUILDROOT_PATH		= $(TOPDIR)/buildroot-$(BUILDROOT_VERSION)
BUILDROOT_TARGET_PATH	= $(BUILDROOT_PATH)/output/target
TARBALL_PATH		= $(TOPDIR)/tarballs
TARGET_BUILD_PATH	= $(TOPDIR)/build
TARGET_PATH		= $(TOPDIR)/target
